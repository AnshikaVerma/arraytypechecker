import java.util.*;

public class ArrayType {

    public static String checkArrayType(int[] arr){
        int countEven = 0,countOdd = 0;
        for(int i=0;i<arr.length;i++){
            if(arr[i]%2 ==0){
                countEven +=1;
            }
            else{
                countOdd += 1;
            }
        }
        if(countEven==0){
            return("Odd");
        }
        else if(countOdd == 0){
            return("Even");
        }
        else{
            return("Mixed");
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int sizeOfArray = sc.nextInt();
        int[] arr = new int[sizeOfArray];
        for(int i=0;i<sizeOfArray;i++){
            arr[i] = sc.nextInt();
        }

        System.out.println(checkArrayType(arr));
    }
}
